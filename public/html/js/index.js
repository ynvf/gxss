 <script>
    new Vue({
      el: "#app",
      data: {
        tableData: [],
        rowStyle: {
          background: 'rgba(255, 255, 255, 0.0)',
          color: 'white'
        },
        cellStyle: {
          background: 'rgba(255, 255, 255, 0.0)',
          'border-bottom': 'none',
          padding: '5px 0',
        },
        map: null,
        nowTime: '',
        facList: [{
          label: '右转危险区',
        }, {
          label: '交通信号灯',
        }, {
          label: '支撑杆件',
        }, {
          label: '交通标志',
        }, {
          label: '交通监控',
        }, {
          label: '交通标线',
        }, {
          label: '其他智能设施',
        }],
        mapFac: '右转危险区', //地图上选中的设施类型
        part4Fac: '其他智能设施', //各中队设施类型数量统计部分选择的类型
        part5Fac: '其他智能设施', //各道路设施类型数量统计部分选择的类型
        char1Data: [
          [],
          []
        ],
        char2Data: [
          [],
          []
        ],
        char3Data: [],
        char4Data: [
          [],
          []
        ],
        char4DataList: [],
        char5Data: [
          [],
          []
        ],
        char5DataList: [],
      },
      created() {
        this.getJson()
        this.showTime()
      },
      mounted() {
        let that = this
        setTimeout(() => {
          that.initial1()
          that.initial2()
          that.initial3()
          that.initial4()
          that.initial5()
          that.initMap()
          setInterval(that.showTime, 1000);
        }, 200)
      },
      watch: {
        part4Fac: {
          handler(newV) {
            for (var i = 0; i < this.char4DataList.length; i++) {
              if (this.char4DataList[i].name == newV) {
                this.char4Data = [
                  [],
                  []
                ]
                for (let ele in this.char4DataList[i].value) {
                  this.char4Data[0].push(ele)
                  this.char4Data[1].push(this.char4DataList[i].value[ele])
                }
                break;
              }
            }
            this.initial4()
          },
          deep: true
        },
        part5Fac: {
          handler(newV) {
            for (var i = 0; i < this.char5DataList.length; i++) {
              if (this.char5DataList[i].name == newV) {
                this.char5Data = [
                  [],
                  []
                ]
                for (let ele in this.char5DataList[i].value) {
                  this.char5Data[0].push(ele)
                  this.char5Data[1].push(this.char5DataList[i].value[ele])
                }
                break;
              }
            }
            this.initial5()
          },
          deep: true
        }
      },
      methods: {
        // 动态展示现在时间
        showTime() {
          var date = new Date();
          var year = date.getFullYear();
          var month = date.getMonth() + 1;
          month = month < 10 ? "0" + month : month;
          var day = date.getDate();
          day = day < 10 ? "0" + day : day;
          var week = "日一二三四五六".charAt(date.getDay()); // 使用charAt函数提取相应汉字
          var hour = date.getHours();
          hour = hour < 10 ? "0" + hour : hour; // 用三目运算符调整数字显示格式
          var minute = date.getMinutes();
          minute = minute < 10 ? "0" + minute : minute;
          var second = date.getSeconds();
          second = second < 10 ? "0" + second : second;
          // 加载现在时间
          var current = hour + ":" + minute + ":" + second;
          this.nowTime = current;
        },
        // 获取json数据
        async getJson() {
          var xhr = new XMLHttpRequest();
          xhr.open('GET', 'json/screen_data.json', true);
          xhr.responseType = 'json';
          let that = this
          xhr.onload = await

          function() {
            if (xhr.status === 200) {
              var data = xhr.response;
              console.log(data)
              that.tableData = []
              JSON.parse(data[0].value).map(item => {
                that.tableData.push(item)
              })
              that.char1Data = [
                [],
                []
              ]
              for (let item in JSON.parse(data[1].value)) {
                that.char1Data[0].push(item)
                that.char1Data[1].push(JSON.parse(data[1].value)[item])
              }
              that.char2Data = [
                [],
                []
              ]
              for (let item in JSON.parse(data[5].value)) {
                that.char2Data[0].push(item)
                that.char2Data[1].push(JSON.parse(data[5].value)[item])
              }
              that.char3Data = []
              for (let item in JSON.parse(data[4].value)) {
                that.char3Data.push({
                  value: JSON.parse(data[4].value)[item],
                  name: item
                })
              }
              that.char4DataList = []
              for (let item in JSON.parse(data[2].value)) {
                that.char4DataList.push({
                  value: JSON.parse(data[2].value)[item],
                  name: item
                })
                if (item == that.part4Fac) {
                  that.char4Data = [
                    [],
                    []
                  ]
                  for (let ele in JSON.parse(data[2].value)[item]) {
                    that.char4Data[0].push(ele)
                    that.char4Data[1].push(JSON.parse(data[2].value)[item][ele])
                  }
                }
              }
              that.char5DataList = []
              for (let item in JSON.parse(data[3].value)) {
                that.char5DataList.push({
                  value: JSON.parse(data[3].value)[item],
                  name: item
                })
                if (item == that.part5Fac) {
                  that.char5Data = [
                    [],
                    []
                  ]
                  for (let ele in JSON.parse(data[3].value)[item]) {
                    that.char5Data[0].push(ele)
                    that.char5Data[1].push(JSON.parse(data[3].value)[item][ele])
                  }
                }
              }
            }
          };
          xhr.send();
        },
        // 交通监控类型统计图表
        initial1() {
          var chartDom = document.getElementById('chart1');
          var myChart = echarts.init(chartDom);
          var option;

          option = {
            tooltip: {
              trigger: 'axis',
              axisPointer: {
                type: 'shadow'
              }
            },
            axisLabel: {
              color: "#fff",
            },
            grid: {
              top: '10%',
              containLabel: true
            },
            xAxis: {
              type: 'category',
              data: this.char1Data[0]
            },
            yAxis: {
              type: 'value'
            },
            series: [{
              data: this.char1Data[1],

              type: 'bar',
              itemStyle: {
                color: "#47F1C2",
              },
            }]
          };
          option && myChart.setOption(option);
        },
        // 国省干线里程数统计
        initial2() {
          var chartDom = document.getElementById('chart2');
          var myChart = echarts.init(chartDom);
          var option;

          option = {
            tooltip: {
              trigger: 'axis',
              axisPointer: {
                type: 'shadow'
              }
            },
            axisLabel: {
              color: "#fff",
            },
            grid: {
              top: '10%',
              containLabel: true
            },
            xAxis: {
              type: 'category',
              boundaryGap: false,
              data: this.char2Data[0]
            },
            yAxis: {
              type: 'value'
            },
            series: [{
              data: this.char2Data[1],
              type: 'line',
              areaStyle: {
                color: "#2BA7E2",
                opacity: 0.2,
              },
              lineStyle: {
                normal: {
                  color: "#2BA7E2",
                  width: 2,
                  type: "solid",
                },
              },
            }]
          };

          option && myChart.setOption(option);
        },
        // 交通标志数量类型图表
        initial3() {
          var chartDom = document.getElementById('chart3');
          var myChart = echarts.init(chartDom);
          var option;

          option = {
            tooltip: {
              trigger: 'item'
            },
            series: [{
              type: 'pie',
              radius: ['50%', '70%'],
              center: ["50%", "41%"],
              emphasis: {
                itemStyle: {
                  shadowBlur: 10,
                  shadowOffsetX: 0,
                  shadowColor: "rgba(0, 0, 0, 0.5)",
                },
              },
              labelLine: {
                show: true,
              },
              label: {
                normal: {
                  formatter: '{b|{b}} {c|{c}}', //图形外文字上下显示
                  borderWidth: 30,
                  borderRadius: 4,
                  padding: [0, 2], //文字和图的边距
                  rich: {
                    a: {
                      color: '#333',
                      fontSize: 14,
                      lineHeight: 30
                    },
                    b: { //name 文字样式
                      fontSize: 14,
                      lineHeight: 40,
                      color: '#CDCDD0',
                    },
                    c: { //value 文字样式
                      fontSize: 14,
                      lineHeight: 40,
                      color: '#CDCDD0',
                      align: "center"
                    }
                  }
                }
              },
              data: this.char3Data
            }]
          };

          option && myChart.setOption(option);
        },
        // 各中队设施类型数量统计图表
        initial4() {
          var chartDom = document.getElementById('chart4');
          var myChart = echarts.init(chartDom);
          var option;

          option = {
            tooltip: {
              trigger: 'axis',
              axisPointer: {
                type: 'shadow'
              }
            },
            grid: {
              top: '17%',
              containLabel: true
            },
            axisLabel: {
              color: "#fff",
            },
            xAxis: {
              type: 'category',
              data: this.char4Data[0]
            },
            yAxis: {
              type: 'value'
            },
            series: [{
              data: this.char4Data[1],
              type: 'bar',
              itemStyle: {
                color: "#1AD2FF",
              },
            }]
          };
          option && myChart.setOption(option);
        },
        // 各道路设施类型数量统计图表
        initial5() {
          var chartDom = document.getElementById('chart5');
          var myChart = echarts.init(chartDom);
          var option;

          option = {
            tooltip: {
              trigger: 'axis',
              axisPointer: {
                type: 'shadow'
              }
            },
            axisLabel: {
              color: "#fff",
            },
            grid: {
              top: '17%',
              containLabel: true
            },
            xAxis: {
              type: 'value',
              boundaryGap: [0, 0.01]
            },
            yAxis: {
              type: 'category',
              data: this.char5Data[0]
            },
            series: [{
              type: 'bar',
              data: this.char5Data[1],
              itemStyle: {
                color: "#21DCA9",
              },
            }, ]
          };
          option && myChart.setOption(option);
        },
        // 地图初始化
        initMap() {
          minemap.domainUrl = "https://minedata.cn";
          minemap.dataDomainUrl = "https://minedata.cn";
          minemap.serverDomainUrl = "https://minedata.cn";
          minemap.spriteUrl = "https://minedata.cn/minemapapi/v2.1.0/sprite/sprite";
          minemap.serviceUrl = "https://minedata.cn/service/";

          /**
           * key、solution设置
           */
          minemap.key = "6ae88a408d6d45b0ac82f37d0626cfe9";
          minemap.solution = 17853;

          /**
           * 初始化地图实例
           */
          this.map = new minemap.Map({
            container: "map",
            style: "https://minedata.cn/service/solu/style/id/17853" /*底图样式*/ ,
            center: [120.65, 31.14] /*地图中心点*/ ,
            zoom: 10 /*地图默认缩放等级*/ ,
            pitch: 0 /*地图俯仰角度*/ ,
            maxZoom: 17 /*地图最大缩放等级*/ ,
            minZoom: 3 /*地图最小缩放等级*/ ,
            projection: "MERCATOR",
          });
        },
      }
    });
  </script>
  