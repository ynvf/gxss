import request from '@/utils/request'

// 新增巡查设备
export function addPatEqu(data) {
    return request({
        url: '/patEqu/insertPat',
        method: 'post',
        data
    })
}

// 根据id查询当前设备
export function searchCurrentPat(id) {
    return request({
        url: '/patEqu/selectPatById',
        method: 'get',
        params: {
            id
        }
    })
}
// 查询所有设备
export function searchAllEqu(id) {
    return request({
        url: '/patEqu/selectPatById',
        method: 'get',
        params: {
            id
        }
    })
}

// 编辑设备
export function editPat(dd) {
    return request({
        url: '/patEqu/updateById',
        method: 'put',
        data
    })
}
