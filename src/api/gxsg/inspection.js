import request from '@/utils/request'

export function getAllPoint(data) {
    return request({
        url: '/test/selectAll',
        method: 'post',
        data
    })
}

export function getAllPointMark() {
    return request({
        url: '/test/selectSign',
        method: 'get'
    })
}
export function selfCheckApi() {
    return request({
        url: `/test/updateIsDel`,
        method: 'put'
    })
}

// 根据id查询对应的交通设施
export function searchFac(id) {
    return request({
        url: `/fac/selectFac`,
        method: 'get',
        params: {
            id
        }
    })
}