import request from '@/utils/request'

// 大屏数据统计
export function getFacNum(deptId) {
    return request({
        url: '/fac/selectFacNum',
        method: 'get',
        params: {
            deptId: deptId
        }
    })
}

// 各中队交通设施数量统计
export function getFacNumType(type) {
    return request({
        url: '/fac/selectFacNumType',
        method: 'get',
        params: {
            type: type
        }
    })
}
// 获取道路交通设施数量排名
export function getFacNumRank(params) {
    return request({
        url: `/fac/selectList`,
        method: 'get',
        params
    })
}

// 大屏 总里程 / 在线设备数量 / 巡逻事件数量统计
export function getScreenBasicData() {
    return request({
        url: `/patEqu/statis`,
        method: 'get'
    })
}

// 获取巡逻事件列表
export function getPatEventList(params) {
    return request({
        url: `/patEqu/patEventList`,
        method: 'get',
        params
    })
}