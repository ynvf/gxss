import request from "@/utils/request";

// 登录方法
export function login(username, password, code, uuid, publicKey) {
  const data = {
    username,
    password,
    code,
    uuid
  };
  return request({
    url: "/login",
    headers: {
      isToken: false,
      publicKey: publicKey,
    },
    method: "post",
    data: data,
  });
}

// 注册方法
export function register(data) {
  return request({
    url: "/register",
    headers: {
      isToken: false,
    },
    method: "post",
    data: data,
  });
}

// 注册上传
export function upload(data) {
  return request({
    url: "/common/company/registerFile",
    headers: {
      isToken: false,
    },
    method: "post",
    data: data,
  });
}


// 获取所有大队信息
export function getSquBri() {
  return request({
    url: "/system/dept/allSquBri?flag=0",
    method: "get",
  });
}

// 获取用户详细信息
export function getInfo() {
  return request({
    url: "/getInfo",
    method: "get",
  });
}

// 退出方法
export function logout() {
  return request({
    url: "/logout",
    method: "post",
  });
}

// 获取验证码
export function getCodeImg() {
  return request({
    url: "/captchaImage",
    headers: {
      isToken: false,
    },
    method: "get",
    timeout: 20000,
  });
}

// 获取sm2公钥
export function getSm2() {
  return request({
    url: "/common/sm2",
    headers: {
      isToken: false,
    },
    method: "get",
  });
}
