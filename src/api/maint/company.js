import request from '@/utils/request'

// 查询养护单位列表
export function listCompany(query) {
  return request({
    url: '/maint/company/list',
    method: 'get',
    params: query
  })
}

// 查询养护单位详细
export function getCompany(id) {
  return request({
    url: '/maint/company/' + id,
    method: 'get'
  })
}

// 新增养护单位
export function addCompany(data) {
  return request({
    url: '/maint/company',
    method: 'post',
    data: data
  })
}

// 修改养护单位
export function updateCompany(data) {
  return request({
    url: '/maint/company',
    method: 'put',
    data: data
  })
}

// 删除养护单位
export function delCompany(id) {
  return request({
    url: '/maint/company/' + id,
    method: 'delete'
  })
}
