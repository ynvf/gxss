import request from '@/utils/request'

// 查询养护费用列表
export function listCost(query) {
  return request({
    url: '/maint/cost/list',
    method: 'get',
    params: query
  })
}

// 查询养护费用详细
export function getCost(id) {
  return request({
    url: '/maint/cost/' + id,
    method: 'get'
  })
}

// 新增养护费用
export function addCost(data) {
  return request({
    url: '/maint/cost',
    method: 'post',
    data: data
  })
}

// 修改养护费用
export function updateCost(data) {
  return request({
    url: '/maint/cost',
    method: 'put',
    data: data
  })
}

// 删除养护费用
export function delCost(id) {
  return request({
    url: '/maint/cost/' + id,
    method: 'delete'
  })
}
