import request from '@/utils/request'

// 查询养护审批列表
export function listReason(query) {
  return request({
    url: '/maint/reason/list',
    method: 'get',
    params: query
  })
}

// 查询养护审批详细
export function getReason(id) {
  return request({
    url: '/maint/reason/' + id,
    method: 'get'
  })
}

// 新增养护审批
export function addReason(data) {
  return request({
    url: '/maint/reason',
    method: 'post',
    data: data
  })
}

// 修改养护审批
export function updateReason(data) {
  return request({
    url: '/maint/reason',
    method: 'put',
    data: data
  })
}

// 删除养护审批
export function delReason(id) {
  return request({
    url: '/maint/reason/' + id,
    method: 'delete'
  })
}
