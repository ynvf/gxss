import request from '@/utils/request'

// 查询养护任务列表
export function listTask(query) {
  return request({
    url: '/maint/task/list',
    method: 'get',
    params: query
  })
}

// 查询养护任务详细
export function getTask(id) {
  return request({
    url: '/maint/task/' + id,
    method: 'get'
  })
}

// 新增养护任务
export function addTask(data) {
  return request({
    url: '/maint/task',
    method: 'post',
    data: data
  })
}

// 修改养护任务
export function updateTask(data) {
  return request({
    url: '/maint/task',
    method: 'put',
    data: data
  })
}

// 删除养护任务
export function delTask(id) {
  return request({
    url: '/maint/task/' + id,
    method: 'delete'
  })
}
