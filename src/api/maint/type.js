import request from '@/utils/request'

// 查询费用种类列表
export function listType(query) {
  return request({
    url: '/maint/type/list',
    method: 'get',
    params: query
  })
}

// 查询费用种类详细
export function getType(id) {
  return request({
    url: '/maint/type/' + id,
    method: 'get'
  })
}

// 新增费用种类
export function addType(data) {
  return request({
    url: '/maint/type',
    method: 'post',
    data: data
  })
}

// 修改费用种类
export function updateType(data) {
  return request({
    url: '/maint/type',
    method: 'put',
    data: data
  })
}

// 删除费用种类
export function delType(id) {
  return request({
    url: '/maint/type/' + id,
    method: 'delete'
  })
}
