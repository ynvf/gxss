import request from '@/utils/request'

// 获取路由
export const getRouters = (param) => {
  return request({
    url: `/getRouters?nonType=${param.nonType}`,
    method: 'get'
  })
}