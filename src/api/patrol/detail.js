import request from '@/utils/request'

// 查询巡查上报设施详细情况列表
export function listDetail(query) {
  return request({
    url: '/patrol/detail/list',
    method: 'get',
    params: query
  })
}

// 查询巡查上报设施详细情况详细
export function getDetail(id) {
  return request({
    url: '/patrol/detail/' + id,
    method: 'get'
  })
}

// 新增巡查上报设施详细情况
export function addDetail(data) {
  return request({
    url: '/patrol/detail',
    method: 'post',
    data: data
  })
}

// 修改巡查上报设施详细情况
export function updateDetail(data) {
  return request({
    url: '/patrol/detail',
    method: 'put',
    data: data
  })
}

// 删除巡查上报设施详细情况
export function delDetail(id) {
  return request({
    url: '/patrol/detail/' + id,
    method: 'delete'
  })
}
