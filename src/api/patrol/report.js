import request from "@/utils/request";

// 查询巡查上报列表
export function listReport(query) {
  return request({
    url: "/patrol/report/list",
    method: "get",
    params: query,
  });
}

// 查询巡查上报详细
export function getReport(id) {
  return request({
    url: "/patrol/report/detail/" + id,
    method: "get",
  });
}

// 新增巡查上报
export function addReport(data) {
  return request({
    url: "/patrol/report",
    method: "post",
    data: data,
  });
}

export function addTask(data) {
  return request({
    url: "/maint/task/add",
    method: "post",
    data: data,
  });
}

// 修改巡查上报
export function updateReport(data) {
  return request({
    url: "/patrol/report/edit",
    method: "put",
    data: data,
  });
}

// 上报大队
export function changeReport(data) {
  return request({
    url: "/patrol/report/change",
    method: "put",
    data: data,
  });
}

// 退回工单
export function backReport(data) {
  return request({
    url: "/patrol/report/back",
    method: "put",
    data: data,
  });
}

// 监理派单审核通过
export function taskPass(data) {
  return request({
    url: "/maint/task/supervisor/audit",
    method: "post",
    data: data,
  });
}

// 大队中队竣工审核
export function taskEndAudit(data) {
  return request({
    url: "/maint/task/end/audit",
    method: "post",
    data: data,
  });
}

// 整改建设
export function rectification(data) {
  return request({
    url: "/maint/task/rectification",
    method: "post",
    data: data,
  });
}

// 删除巡查上报
export function delReport(id) {
  return request({
    url: "/patrol/report/" + id,
    method: "delete",
  });
}

//获取监理或养护单位列表
export const getServicer = (param) => {
  return request({
    url: `/system/user/getServicer?deptId=${param.deptId}&type=${param.type}`,
    method: "get",
  });
};

//获取设备枚举类型和设备问题列表
export function getFacProblemList() {
  return request({
    url: "/patrol/report/getFacProblemList",
    method: "get",
  });
}

// 图片预览
export const preview = (id) => {
  return request({
    url: `/common/preview?fileUuid=${id}`,
    method: "get",
  });
};

// 图片预览
export function fileList(id) {
  return request({
    url: `/system/file/list?uuid=${id}`,
    method: "get",
  });
}

// 文件记录是否存在
export function checkIsExist(id) {
  return request({
    url: "/system/file/checkIsExist/" + id,
    method: "get",
  });
}

export const deleteFile = (id) => {
  return request({
    url: "/common/system/delete/" + id,
    method: "delete",
  });
};

// 巡查工单派发给外部单位
export const dispatchTask = (param) => {
  return request({
    url: `/maint/task/dispatch?id=${param.id}&outsideId=${param.outsideId}`,
    method: "post",
  });
};

//查询养护工单
export const getTask = (param) => {
  return request({
    url: "/maint/task/list",
    method: "get",
    params: param,
  });
};

//查询养护工单详情
export const getTaskDetail = (id) => {
  return request({
    url: `/maint/task/query?taskId=${id}`,
    method: "get",
  });
};

// 发起竣工
export function taskEnd(data) {
  return request({
    url: "/maint/task/end/unitRequest",
    method: "post",
    data: data,
  });
}

// 上传
export function sysUpload(data) {
  return request({
    url: "/common/system/upload",
    method: "post",
    data: data,
  });
}

// 新增巡查
export function addPatrol(data) {
  return request({
    url: "/patrol/report/add",
    method: "post",
    data: data,
  });
}

//查询养护工单超期
export function overdue(param) {
  return request({
    url: "/maint/task/overdue/list",
    method: "get",
    params: param,
  });
}

//查询养护工单申请延期
export function delayList(param) {
  return request({
    url: "/maint/task/delay/list",
    method: "get",
    params: param,
  });
}

//养护工单延期申请
export function taskDelay(data) {
  return request({
    url: "/maint/task/delay",
    method: "post",
    data: data,
  });
}

//延期申请审核
export function delayAudit(data) {
  return request({
    url: "/maint/task/delay/audit",
    method: "post",
    data: data,
  });
}

//超期工单 图表接口
export function overdueChart(data) {
  return request({
    url: "/maint/task/overdue/chart",
    method: "get",
    params: data,
  });
}
