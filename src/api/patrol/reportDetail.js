import request from '@/utils/request'

// 查询巡查上报与设施详情关联列表
export function listReportDetail(query) {
  return request({
    url: '/patrol/reportDetail/list',
    method: 'get',
    params: query
  })
}

// 查询巡查上报与设施详情关联详细
export function getReportDetail(id) {
  return request({
    url: '/patrol/reportDetail/' + id,
    method: 'get'
  })
}

// 新增巡查上报与设施详情关联
export function addReportDetail(data) {
  return request({
    url: '/patrol/reportDetail',
    method: 'post',
    data: data
  })
}

// 修改巡查上报与设施详情关联
export function updateReportDetail(data) {
  return request({
    url: '/patrol/reportDetail',
    method: 'put',
    data: data
  })
}

// 删除巡查上报与设施详情关联
export function delReportDetail(id) {
  return request({
    url: '/patrol/reportDetail/' + id,
    method: 'delete'
  })
}
