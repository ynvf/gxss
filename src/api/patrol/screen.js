import request from '@/utils/request'

// 近三天养护任务数量
export function getThreeDays() {
  return request({
    url: '/screen/threeDays',
    headers: {
      isToken: false,
    },
    method: 'get'
  })
}

// 总计养护任务数量
export function getAllMaint() {
  return request({
    url: '/screen/allMaint',
    headers: {
      isToken: false,
    },
    method: 'get'
  })
}

// 各中队巡查上报数量排名
export function getGroupByDept(param) {
  return request({
    url: `/screen/groupByDept?beginTime=${param.beginTime}&endTime=${param.endTime}`,
    headers: {
      isToken: false,
    },
    method: 'get'
  })
}

// 道路巡查上报数量排名
export function getGroupByRoad(param) {
  return request({
    url: `/screen/groupByRoad?beginTime=${param.beginTime}&endTime=${param.endTime}`,
    headers: {
      isToken: false,
    },
    method: 'get'
  })
}

// 养护任务状态管理
export function getGroupByType(param) {
  return request({
    url: `/screen/groupByType?beginTime=${param.beginTime}&endTime=${param.endTime}`,
    headers: {
      isToken: false,
    },
    method: 'get'
  })
}

// 各类型养护任务数量统计
export function getGroupByFac(param) {
  return request({
    url: `/screen/groupByFac?beginTime=${param.beginTime}&endTime=${param.endTime}`,
    headers: {
      isToken: false,
    },
    method: 'get'
  })
}

// 今日养护任务展示
export function getTodayMaint() {
  return request({
    url: '/screen/todayMaint',
    headers: {
      isToken: false,
    },
    method: 'get'
  })
}

export function queryFacList(param) {
  return request({
    url: `/fac/queryFacList?facTypes=${param}`,
    method: 'get'
  })
}


export function queryFacDetail(query) {
  return request({
    url: '/fac/queryFacDetail',
    method: 'get',
    params: query
  })
}
