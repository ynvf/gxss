import request from '@/utils/request'

// 查询巡查上报设施种类列表
export function listType(query) {
  return request({
    url: '/patrol/type/list',
    method: 'get',
    params: query
  })
}

// 查询巡查上报设施种类详细
export function getType(id) {
  return request({
    url: '/patrol/type/' + id,
    method: 'get'
  })
}

// 新增巡查上报设施种类
export function addType(data) {
  return request({
    url: '/patrol/type',
    method: 'post',
    data: data
  })
}

// 修改巡查上报设施种类
export function updateType(data) {
  return request({
    url: '/patrol/type',
    method: 'put',
    data: data
  })
}

// 删除巡查上报设施种类
export function delType(id) {
  return request({
    url: '/patrol/type/' + id,
    method: 'delete'
  })
}
