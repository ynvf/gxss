import Cookies from 'js-cookie'

const TokenKey = 'Admin-Token'

const LoginKey = 'Login-Key'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

export function getLoginKey() {
  return Cookies.get(LoginKey)
}

export function setLoginKey(key) {
  return Cookies.set(LoginKey, key)
}

export function removeLoginKey() {
  return Cookies.remove(LoginKey)
}
