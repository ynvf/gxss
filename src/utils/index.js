import Vue from 'vue'
import router from '@/router'
import store from '@/store'
// import storage from '@/utils/storage'

/**
 * 获取uuid
 */
export function getUUID () {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
    return (c === 'x' ? (Math.random() * 16 | 0) : ('r&0x3' | '0x8')).toString(16)
  })
}

/**
 * 获取简单的uuid
 */
export function getSimpleUUID () {
  return 'xxxxxxxyxxxx4xxxyxxxxxxyxxxxxyxx'.replace(/[xy]/g, c => {
    return (c === 'x' ? (Math.random() * 16 | 0) : ('r&0x3' | '0x8')).toString(16)
  })
}

/**
 * 管理后台 - 是否有权限
 * @param {*} key
 */
export function isAuth (key) {
  return JSON.parse(sessionStorage.getItem('permissions') || '[]').indexOf(key) !== -1 || false
}

/**
 * 业务前台 - 是否有权限
 * @param {*} key
 */
export function isFrontAuth (key) {
  return JSON.parse(sessionStorage.getItem('frontPermissions') || '[]').indexOf(key) !== -1 || false
}

/**
 * 树形数据转换
 * @param {*} data
 * @param {*} id
 * @param {*} pid
 */
export function treeDataTranslate (data, id = 'id', pid = 'parentId') {
  var res = []
  var temp = {}
  for (var i = 0; i < data.length; i++) {
    temp[data[i][id]] = data[i]
  }
  for (var k = 0; k < data.length; k++) {
    if (temp[data[k][pid]] && data[k][id] !== data[k][pid]) {
      if (!temp[data[k][pid]]['children']) {
        temp[data[k][pid]]['children'] = []
      }
      if (!temp[data[k][pid]]['_level']) {
        temp[data[k][pid]]['_level'] = 1
      }
      data[k]['_level'] = temp[data[k][pid]]._level + 1
      temp[data[k][pid]]['children'].push(data[k])
    } else {
      res.push(data[k])
    }
  }
  return res
}

/**
 * 获取登录的token信息
 */
export function getLoginInfo () {
  return Vue.cookie.get('token')
  // return JSON.parse(sessionStorage.getItem('token'))
  // return JSON.parse(storage.getItem('token'))
}

/**
 * 清除登录信息
 */
export function clearLoginInfo () {
  Vue.cookie.delete('token')
  // sessionStorage.removeItem('token')
  store.commit('resetStore')
  router.options.isAddDynamicMenuRoutes = false
}

/**
 * 获取标志牌类型
 */
export function getSignType (typeCode) {
  switch (typeCode) {
    case 1:
      return "旅游区标志";
    case 2:
      return "指示标志";
    case 3:
      return "辅助标志";
    case 4:
      return "警告标志";
    case 5:
      return "禁令标志";
    case 6:
      return "指路标志";
    case 7:
      return "干路和支路指路标志";
    case 8:
      return "告示标志";
    case 9:
      return "组合标志";
    case 10:
      return "作业区标志";
    case 11:
      return "快速路指路标志";
    case 12:
      return "其他标志";
    default:
      "其他标志";
  }
  // switch (typeCode) {
  //   case 0:
  //     return '指示标志'
  //   case 1:
  //     return '禁令标志'
  //   case 2:
  //     return '警告标志'
  //   case 3:
  //     return '快速路指路标志'
  //   case 4:
  //     return '辅助标志'
  //   case 5:
  //     return '旅游区标志'
  //   case 6:
  //     return '干路和支路指路标志'
  //   case 7:
  //     return '其他标志'
  //   case 8:
  //     return '作业区标志'
  //   case 9:
  //     return '路名牌'
  //   case 10:
  //     return '告示标志'
  //   case 11:
  //     return '警告标志|辅助标志'
  //   case 12:
  //     return '干路和支路指路标志|辅助标志'
  //   case 13:
  //     return '辅助标志|黑底'
  //   case 14:
  //     return '禁令标志|辅助标志'
  //   case 15:
  //     return '辅助标志|快速路指路标志'
  //   case 16:
  //     return '指示标志|辅助标志'
  //   case 17:
  //     return '干路和支路指路标志|告示标志'
  //   case 18:
  //     return '告示标志|警告标志'
  //   case 19:
  //     return '禁令标志|警告标志'
  //   case 20:
  //     return '禁令标志|快速路指路标志'
  //   case 21:
  //     return '禁令标志|其他标志'
  //   case 22:
  //     return '警告标志|其他标志'
  //   case 23:
  //     return '警告标志|黄底'
  //   case 24:
  //     return '警告标志|指示标志'
  //   case 25:
  //     return '指示标志|干路和支路指路标志'
  //   case 26:
  //     return '指示标志|黑底'
  //   case 27:
  //     return '指示标志|红底'
  //   case 28:
  //     return '指示标志|禁令标志'
  //   case 29:
  //     return '指示标志|蓝底'
  //   case 30:
  //     return '作业区标志|辅助标志'
  //   default:
  //     return '其他标志'
  // }
}

/**
 * 获取信号灯类型
 */
export function getLightType (typeCode) {
  switch (typeCode) {
    case 0:
      return '人行横道'
    case 1:
      return '车行道'
    case 2:
      return '非机动车'
    case 3:
      return '机动车'
    default:
      return '其他'
  }
}

/**
 * 获取支撑杆件类型
 */
export function getSupportType (typeCode) {
  switch (typeCode) {
    case 0:
      return '机动车道'
    case 1:
      return '非机动车道'
    default:
      return '其他'
  }
}

/**
 * 获取-道路监控类型
 */
export function getMonitorType (typeCode) {
  switch (typeCode) {
    case 0:
      return '电子眼'
    case 1:
      return '天眼'
    default:
      return '其他'
  }
}

/**
 * 获取-道路护栏类型
 */
export function getRoadGType (typeCode) {
  switch (typeCode) {
    case 0:
      return '隔离护栏'
    case 1:
      return '防撞护栏'
    default:
      return '其他'
  }
}

/**
 * 获取-道路标线类型
 */
export function getIndexLineType (typeCode) {
  switch (typeCode) {
    case 0:
      return '车道线'
    case 1:
      return '指示标线'
    default:
      return '其他'
  }
}

/**
 * 获取-人行横道线类型
 */
export function getWalkLineType (typeCode) {
  switch (typeCode) {
    case 0:
      return '无信号灯'
    case 1:
      return '有信号灯'
    default:
      return '其他'
  }
}

/**
 * 获取-人行横道线类型
 */
export function getCrossFacType (typeCode) {
  switch (typeCode) {
    case 0:
      return '天桥'
    case 1:
      return '地道'
    default:
      return '其他'
  }
}

/**
 * 获取-公交站台类型
 */
export function getBusType (typeCode) {
  switch (typeCode) {
    case 0:
      return '港湾式'
    case 1:
      return '直线式'
    case 2:
      return '临停式'
    default:
      return '其他'
  }
}

export function getQrCodeUrl (typeCode) {
  // return 'https://www.boyoutx.cn/ycewm'
  return 'https://yc.beliefdatatech.com/ycewm'
}
