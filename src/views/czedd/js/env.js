const env = {
  map: null,
  map2D: null,
  map2DEdit: null,
  map2DUtil: null,
  map3D: null,
  map3DEdit: null,
  map3DUtil: null,
  insHeatMap: null,
  _marker: null
}

export default env
